import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { WeatherService } from '../services/weather.service';
import { Meteo } from '../models/meteo';
import { FormGroup, FormControl, Validator } from '@angular/forms';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.scss']
})
export class WeatherComponent implements OnInit {

  @ViewChild('search-txt') qElementRef: ElementRef;

  constructor(private weatherService: WeatherService) { }

  cityForm = new FormGroup({
    city: new FormControl(''),
  });
  meteo = new Meteo();
  data = null;
  city = '';
  
  onClick() {
    
    this.city = this.meteo.city;

    this.data = this.weatherService.getWeather(this.city).subscribe(
      (apiData) => {
        this.data = apiData;
        console.log(this.data);
      }
    );
    console.log(this.city)
  }

  ngOnInit() {
    const placesAutocomplete = places({
      appId: 'plXMKR7C7KD8',
      apiKey: 'c64742f0e54eb1884a132ae5b3ba2a15',
      container: document.getElementById('search-txt')   
    });

    placesAutocomplete.on('suggesti', e => {
      this.meteo.city = e.suggestion.name;
    });

    placesAutocomplete.on('change', e => {
      this.meteo.city = e.suggestion.name;
    });
  }

}
